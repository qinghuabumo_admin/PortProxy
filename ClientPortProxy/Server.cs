namespace ClientPortProxy
{
	using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Net;
	using System.Net.Sockets;
	using System.Runtime.InteropServices;
    using System.Text;
    using System.Threading.Tasks;

	using Microsoft.Extensions.DependencyInjection;
	using Microsoft.Extensions.Logging;

	using NLog;
    using ClientPortProxy.Model;
    using System.Threading;

    public class Server
	{
        private Dictionary<int, TcpListener> ListenerDict = new Dictionary<int, TcpListener>();
		private ILogger<Server> _logger;
        private ProxyInfo ProxyInfo;
        private IServiceProvider _serviceProvider;

		public Server(ILogger<Server> logger, IServiceProvider serviceProvider)
		{
			_logger = logger;
			_serviceProvider = serviceProvider;

			Debug.Assert(_serviceProvider != null, "No IServiceProvider found.");
		}

        public void Init(int bufferSize, int port, bool local, string remoteServer, int remoteServerPort)
        {
            ProxyInfo.BufferSize = bufferSize;
            ProxyInfo.ListenPort.Add(port);
            ProxyInfo.IsLocal = local;
            ProxyInfo.ServerIp = remoteServer;
            ProxyInfo.ServerPort = remoteServerPort;
        }
        public void Init(ProxyInfo proxyInfo)
        {
            ProxyInfo = proxyInfo;
        }

        /// <summary>
        /// 获得或设置是否是本地模式
        /// </summary>
        public bool Local => ProxyInfo.IsLocal;

        public Dictionary<int, ManualResetEvent> tcpClientConnected = new Dictionary<int, ManualResetEvent>();
        public void Start()
        {
            var _local = ProxyInfo.IsLocal;

            foreach (var port in ProxyInfo.ListenPort)
            {
                tcpClientConnected.Add(port, new ManualResetEvent(false));
                Task.Run(() =>
                {
                    var _port = port;
                    _logger.LogInformation("正在启动监听...");
                    TcpListener listener = null;
                    try
                    {
                        listener = new TcpListener(IPAddress.Any, _port);
                        listener.Start();
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, $"端口监听异常 {_port}");
                        return;
                    }
                    _logger.LogInformation($"监听端口 {_port}, 本地模式 {_local}...");
                    _logger.LogInformation("等待客户端连接...");
                    var connectionCount = 0L;
                    while (true)
                    {
                        connectionCount++;
                        try
                        {
                            // Set the event to nonsignaled state.
                            tcpClientConnected[_port].Reset();

                            // Start to listen for connections from a client.
                            Console.WriteLine("Waiting for a connection...");

                            listener.BeginAcceptTcpClient(new AsyncCallback(DoAcceptTcpClientCallback), new BeginAcceptTcpClientModel() { ConnectionCount = connectionCount, Port = _port, ListenerSocket = listener });

                            tcpClientConnected[_port].WaitOne();
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex,"数据处理异常");
                        }
                        //等待请求
                        //var client = listener.AcceptTcpClientAsync().Result;
                        //connectionCount++;
                        //_logger.LogInformation($"#{connectionCount} 新的客户端连接 {client.Client.RemoteEndPoint} -> {client.Client.LocalEndPoint}");
                        ////处理客户请求
                        //ProcessClientAsync(connectionCount, client, _port);
                    }
                });
            }
            while (true)
            {
                Thread.Sleep(1000);
            }
        }
        private void DoAcceptTcpClientCallback(IAsyncResult ar)
        {
            try
            {
                var model = (BeginAcceptTcpClientModel)ar.AsyncState;

                TcpClient client = model.ListenerSocket.EndAcceptTcpClient(ar);

                ProcessClientAsync(model.ConnectionCount, client, model.Port);

                tcpClientConnected[model.Port].Set();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "DoAcceptTcpClientCallback异常");
            }
        }

        async void ProcessClientAsync(long connectionCount, TcpClient client,int CurrentListenPort)
		{
            List<byte> requestByteData = new List<byte>();
            //创建流转换器
			IStreamTransformer CreateStreamTransformer(int key, int keyIndex)
			{
				var t = _serviceProvider.GetRequiredService<IStreamTransformer>();
				t.Init(key, keyIndex);
				return t;
			}

            //客户端请求的数据
            var stream = client.GetStream();

            stream.ReadTimeout = 60000; // 设置读取超时时间为10秒
            stream.WriteTimeout = 60000; // 设置写入超时时间为10秒
            int count = 100;
            string str = "";
            while (true)
            {
                try
                {
                    byte[] buffer1 = new byte[500];
                    count = stream.Read(buffer1, 0, 500);
                    if (count == 0 || count < 500)
                    {
                        byte[] newByte = new byte[count];
                        Array.Copy(buffer1, newByte, count);
                        requestByteData.AddRange(newByte);
                        str += Encoding.UTF8.GetString(newByte);
                        break;
                    }
                    else
                    {
                        str += Encoding.UTF8.GetString(buffer1, 0, count);
                        requestByteData.AddRange(buffer1);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogInformation($"#{connectionCount} 异常:" + ex);
                    break;
                }
            }

            //整理请求报文  4字节端口 + 转发数据
            byte[] portBytes = BitConverter.GetBytes(CurrentListenPort);
            requestByteData.InsertRange(0, portBytes);


            _logger.LogInformation($"#{connectionCount} 请求数据:" + str);
            
            var port = ProxyInfo.IsLocal ? ProxyInfo.ServerPort : CurrentListenPort;

			var valid = false;
			var validator = _serviceProvider.GetRequiredService<IStreamValidator>();
			if (!Local)
            {
                //如果不是本地模式就去做验证
                _logger.LogInformation($"#{connectionCount} 正在验证");
                try
				{
					_logger.LogInformation($"#{connectionCount} 正在验证请求");
					valid = await validator.Validate(stream, port);
				}
				catch (Exception e)
				{
					_logger.LogInformation($"#{connectionCount} 验证异常：{e.Message}");
				}
				finally
				{
					_logger.LogInformation($"#{connectionCount} 验证结果：{valid}");
                    //验证失败就把这个请求给关闭掉
					if (!valid)
					{
						client.Close();
					}
				}
			}
			else
			{
				valid = true;
			}

			//远程服务器
			var upclient = new TcpClient();
			NetworkStream upstream = null;
			if (valid)
			{
				try
				{
					_logger.LogInformation($"#{connectionCount} 正在连接上游服务器");
                    //连接远程服务器
					await upclient.ConnectAsync(ProxyInfo.ServerIp, ProxyInfo.ServerPort);
					upstream = upclient.GetStream();
					_logger.LogInformation($"#{connectionCount} 上游服务器连接已打开 {upclient.Client.LocalEndPoint} -> {upclient.Client.RemoteEndPoint}");
					if (ProxyInfo.IsLocal)
					{
                        //如果是本地模式 生成验证数据 请求到服务端  如果服务端不通过就会被之前关闭这个请求
						var buffer = validator.GenerateValiationData(port);
						await upstream.WriteAsync(buffer, 0, buffer.Length);
					}
				}
				catch (Exception e)
				{
					_logger.LogError($"#{connectionCount} 未能为打开上游服务器连接: {e.Message}");
					client.Close();
					valid = false;
				}

				if (valid)
				{
                    //数据交换
					await Task.WhenAny(
                        //用客户端请求的数据  去请求服务端
						ProcessStreamCopyAsync(requestByteData, upstream, ProxyInfo.IsLocal ? null : CreateStreamTransformer(port, 1), ProxyInfo.IsLocal ? CreateStreamTransformer(port, 1) : null),
                        //服务端返回的数据 返回给客户端
						ProcessStreamCopyAsync(upstream, stream, ProxyInfo.IsLocal ? CreateStreamTransformer(port, 0) : null, ProxyInfo.IsLocal ? null : CreateStreamTransformer(port, 0))
					);
				}
			}

			try
			{
				upstream?.Dispose();
				stream.Dispose();
				upclient.Client.Close();
				client.Client.Close();
				upclient.Close();
				client.Close();
			}
			catch (Exception e)
			{
				_logger.LogError($"#{connectionCount} 尝试关闭连接的时候发生错误 {e.Message}");
			}

			_logger.LogInformation($"#{connectionCount} 连接已关闭");
		}

		bool IsSocketConnected(Socket socket)
		{
			if (!socket.Connected)
				return false;

			return !(socket.Poll(0, SelectMode.SelectRead) && socket.Available == 0);
		}
        /// <summary>
        /// 数据转发
        /// </summary>
        /// <param name="srcStream">原文件流</param>
        /// <param name="dstStream">新文件流</param>
        /// <param name="readTransformer">读</param>
        /// <param name="writeTransformer">写</param>
        /// <returns></returns>
		async Task ProcessStreamCopyAsync(NetworkStream srcStream, NetworkStream dstStream, IStreamTransformer readTransformer, IStreamTransformer writeTransformer)
        {
            var count = 0;
            var buffer = new byte[ProxyInfo.BufferSize];
            var str = "";
            //srcStream.Seek(0, System.IO.SeekOrigin.Begin);

            do
            {
                try
                {
                    //获取获取到的数据
                    count = await srcStream.ReadAsync(buffer, 0, buffer.Length);
                    if (count == 0)
                    {
                        continue;
                    }
                    //解密
                    readTransformer?.Decode(buffer, 0, count);
                    //加密
                    writeTransformer?.Encode(buffer, 0, count);
                    //返回获取到的数据
                    await dstStream.WriteAsync(buffer, 0, count);
                }
                catch (Exception e)
                {
                    break;
                }
            } while (count > 0);
        }
        async Task ProcessStreamCopyAsync(string srcStream, NetworkStream dstStream, IStreamTransformer readTransformer, IStreamTransformer writeTransformer)
        {
            var count = 0;
            var buffer = Encoding.UTF8.GetBytes(srcStream);
            var str = "";
            //srcStream.Seek(0, System.IO.SeekOrigin.Begin);

            do
            {
                try
                {
                    count = buffer.Length;
                    //解密
                    readTransformer?.Decode(buffer, 0, count);
                    //加密
                    writeTransformer?.Encode(buffer, 0, count);
                    //返回获取到的数据
                    await dstStream.WriteAsync(buffer, 0, count);
                }
                catch (Exception e)
                {
                    break;
                }
            } while (count > 0);
        }

        async Task ProcessStreamCopyAsync(List<byte> srcStream, NetworkStream dstStream, IStreamTransformer readTransformer, IStreamTransformer writeTransformer)
        {
            var count = 0;
            var buffer = srcStream.ToArray();
            var str = "";
            //srcStream.Seek(0, System.IO.SeekOrigin.Begin);

            do
            {
                try
                {
                    count = buffer.Length;
                    //解密
                    readTransformer?.Decode(buffer, 0, count);
                    //加密
                    writeTransformer?.Encode(buffer, 0, count);
                    //返回获取到的数据
                    await dstStream.WriteAsync(buffer, 0, count);
                }
                catch (Exception e)
                {
                    break;
                }
            } while (count > 0);
        }
    }
}

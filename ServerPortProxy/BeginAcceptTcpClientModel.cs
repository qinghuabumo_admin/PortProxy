﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace ServerPortProxy
{
    public class BeginAcceptTcpClientModel
    {
        public long ConnectionCount { get; set; }
        public int Port { get; set; }
        public TcpListener ListenerSocket { get; set; }
    }
}

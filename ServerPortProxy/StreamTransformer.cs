namespace ServerPortProxy
{
	using System;
	using System.Net;

	public class StreamTransformer : IStreamTransformer
	{
		public void Init(int keySource, int keyIndex)
		{
		}

		public void Encode(byte[] buffer, int offset, int length)
		{
		}

		public void Decode(byte[] buffer, int offset, int length)
		{
		}
	}
}

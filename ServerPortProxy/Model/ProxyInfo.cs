﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServerPortProxy.Model
{
    public class ProxyInfo
    {
        /// <summary>
        /// 本地模式  客户端（多个端口监控统一请求到一个端口，请求带上验证参数）  服务端（一个端口拿到数据转发多个端口，拿到数据需要验证请求参数）
        /// </summary>
        public bool IsLocal { get; set; }
        /// <summary>
        /// 监听端口
        /// </summary>
        public List<int> ListenPort { get;} = new List<int>();
        /// <summary>
        /// 服务端IP
        /// </summary>
        public string ServerIp { get; set; }
        /// <summary>
        /// 服务端端口
        /// </summary>
        public int ServerPort { get; set; }
        /// <summary>
        /// 数据流最大长度
        /// </summary>
        public int BufferSize { get; set; }
    }
}

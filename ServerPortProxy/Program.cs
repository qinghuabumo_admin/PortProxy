using System;

namespace ServerPortProxy
{
	using System.Collections.Generic;
	using System.Globalization;
    using System.IO;
    using System.Linq;
	using System.Reflection;
	using System.Threading;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
	using Microsoft.Extensions.Logging;

	using NLog;
	using NLog.Extensions.Logging;
    using ServerPortProxy.Model;

    class Program
	{
		static void Main(string[] args)
		{
			//# corefx bug of https://github.com/dotnet/corefx/issues/24832
			new ArgumentException();

			ConfigTraditionalLog();
			var serviceProvider = BuildDI();
            //log
            var logger = serviceProvider.GetRequiredService<ILogger<Program>>();


            var config = serviceProvider.GetRequiredService<IConfiguration>();
            ProxyInfo proxyInfo = config.GetSection("proxyConfig").Get<ProxyInfo>();

            var srv = serviceProvider.GetRequiredService<Server>();
            //初始化转发数据
			srv.Init(proxyInfo);
            //启动
            srv.Start();

			while (true)
			{
				Thread.Sleep(100);
			}
		}

		static string GetOptionValue(string str)
		{
			if (string.IsNullOrEmpty(str))
				return null;
			return str.Substring(str.IndexOf('=') + 1);
		}

        /// <summary>
        /// 配置LOG
        /// </summary>
		static void ConfigTraditionalLog()
		{
			//workaround for log callsite bug (see https://github.com/NLog/NLog.Extensions.Logging/issues/165)
			LogManager.AddHiddenAssembly(Assembly.Load(new AssemblyName("Microsoft.Extensions.Logging")));
			LogManager.AddHiddenAssembly(Assembly.Load(new AssemblyName("Microsoft.Extensions.Logging.Abstractions")));
			LogManager.AddHiddenAssembly(Assembly.Load(new AssemblyName("NLog.Extensions.Logging")));
		}

        /// <summary>
        /// 依赖注入初始化
        /// </summary>
        /// <returns></returns>
        private static IServiceProvider BuildDI()
        {
            var services = new ServiceCollection();


            var configuration = Configuration();
            services.AddSingleton<IConfiguration>(configuration);

            services.AddTransient<Server>();
            services.AddTransient<IStreamValidator, StreamValidator>();
            services.AddTransient<IStreamTransformer, StreamTransformer>();

            services.AddSingleton<ILoggerFactory, LoggerFactory>();
            services.AddSingleton(typeof(ILogger<>), typeof(Logger<>));

            var serviceProvider = services.BuildServiceProvider();

            var loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>();
            loggerFactory.AddNLog(new NLogProviderOptions { CaptureMessageTemplates = true, CaptureMessageProperties = true });
            loggerFactory.ConfigureNLog("nlog.config");

            return serviceProvider;
        }

        /// <summary>
        /// 构建配置文件
        /// </summary>
        /// <returns></returns>
        private static IConfiguration Configuration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Path.Combine(AppContext.BaseDirectory))
                .AddJsonFile("config.json", false);

            return builder.Build();
        }
    }
}
